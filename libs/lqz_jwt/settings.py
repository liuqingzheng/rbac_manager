
from datetime import timedelta
from django.conf import settings
ACCESS_TOKEN_LIFETIME = timedelta(minutes=5)  # 过期时间
ALGORITHM= "HS256"  # 加密方式
SIGNING_KEY= settings.SECRET_KEY  # 加密key
