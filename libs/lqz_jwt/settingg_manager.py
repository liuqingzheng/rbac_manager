'''
author:lqz
description：用户如果在django的配置文件中配置了LQZ_JWT字典，优先使用用户配置的，如果没配置，使用此处内置的
'''

from django.conf import settings
from . import settings as DEFAULTS

USER_SETTINGS = getattr(settings, "LQZ_JWT", None)

class JWTSettings:
    def __init__(self):
        # 优先在此处的defaults中获取
        for setting in dir(DEFAULTS):
            if setting.isupper():
                setattr(self, setting, getattr(DEFAULTS, setting))
        # 再用django项目中的配置文件覆盖一次
        for setting in USER_SETTINGS.keys():
            if setting.isupper():
                setattr(self, setting, USER_SETTINGS[setting])


# 使用jwt_settings时，优先使用django项目中配置的，如果没有，使用此处内置的
jwt_settings = JWTSettings()
