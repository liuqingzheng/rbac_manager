import os
from pathlib import Path
import sys

BASE_DIR = Path(__file__).resolve().parent.parent.parent
# 把apps路径加入环境变量
sys.path.insert(0, BASE_DIR)
sys.path.insert(0, os.path.join(BASE_DIR, 'apps'))

SECRET_KEY = 'django-insecure-h(gdfubah7v#la)r#$)9z&e*b8b1hoj_ax+yv31gil$=k5t==d'
DEBUG = True

ALLOWED_HOSTS = []

INSTALLED_APPS = [
    'corsheaders',
    # 'rest_framework_simplejwt',
    'rest_framework',
    'user',
    'permission',
    'operationlog',
    'cmdb',
]
MIDDLEWARE = [
    # 'django.middleware.security.SecurityMiddleware',
    # 'django.middleware.common.CommonMiddleware',
    'corsheaders.middleware.CorsMiddleware',
]
ROOT_URLCONF = 'rabc_manager.urls'
TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [BASE_DIR / 'templates'],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.template.context_processors.debug',
                'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]
WSGI_APPLICATION = 'rabc_manager.wsgi.application'
db_user = os.environ.get('DB_USER', 'root')
db_password = os.environ.get('DB_PASSWORD', '123')
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'rbac_manager',
        'USER': db_user,
        'PASSWORD': db_password,
        'HOST': '127.0.0.1',
        'PORT': 3306
    }
}

LANGUAGE_CODE = 'zh-hans'
TIME_ZONE = 'Asia/Shanghai'
USE_I18N = True
USE_L10N = True
USE_TZ = False

STATIC_URL = '/static/'
# media配置
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')



DEFAULT_AUTO_FIELD = 'django.db.models.BigAutoField'


# 日志相关配置

LOGGING = {
    'version': 1,
    'disable_existing_loggers': False,
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s %(lineno)d %(message)s'
        },
        'simple': {
            'format': '%(levelname)s %(module)s %(lineno)d %(message)s'
        },
    },
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
    },
    'handlers': {
        'console': {
            # 实际开发建议使用WARNING
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
            'formatter': 'simple'
        },
        'file': {
            # 实际开发建议使用ERROR，记录到文件中，使用级别高一些
            'level': 'INFO',
            'class': 'logging.handlers.RotatingFileHandler',
            # 日志位置,日志文件名,日志保存目录必须手动创建，
            'filename': os.path.join(BASE_DIR, "logs", "rbac.log"),
            # 日志文件的最大值,这里我们设置300M
            'maxBytes': 300 * 1024 * 1024,
            # 日志文件的数量,设置最大日志数量为10
            'backupCount': 10,
            # 日志格式:详细格式
            'formatter': 'verbose',
            # 文件内容编码
            'encoding': 'utf-8'
        },
    },
    # 日志对象
    'loggers': {
        'django': {
            'handlers': ['console', 'file'],
            'propagate': True,  # 是否让日志信息继续冒泡给其他的日志处理系统
        },
    }
}

# drf配置
REST_FRAMEWORK = {
    "UNAUTHENTICATED_USER": None,
    # 只要出异常，就会执行exception_handler
    'EXCEPTION_HANDLER': 'utils.common_exception.exception_handler',
}

# 开放media访问
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# 跨域配置
CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
    'VIEW',
)

CORS_ALLOW_HEADERS = (
    'XMLHttpRequest',
    'X_FILENAME',
    'accept-encoding',
    'authorization',
    'content-type',
    'dnt',
    'origin',
    'user-agent',
    'x-csrftoken',
    'x-requested-with',
    'Pragma',
)


# jwt 配置
from datetime import timedelta

LQZ_JWT = {
    'ACCESS_TOKEN_LIFETIME': timedelta(days=7)
}




AUTH_USER_MODEL='user.UserInfo'




# ssh远程存储目录
CLIENT_COLLECT_DIR = "/tmp/"

# windows ssh远程存储目录
CLIENT_COLLECT_WIN_DIR = "C:\\Users\\lqz\\Desktop\\"
