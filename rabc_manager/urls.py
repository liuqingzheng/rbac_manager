from django.urls import path,include
from django.views.static import serve
urlpatterns = [
       path('api/v1/user/', include('user.urls')),
       path('api/v1/permission/', include('permission.urls')),
       path('api/v1/cmdb/', include('cmdb.urls')),
       path('media/<path:path>', serve, {}),
]
