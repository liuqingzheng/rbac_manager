/*
 Navicat Premium Data Transfer

 Source Server         : 本地
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : localhost:3306
 Source Schema         : rbac_manager

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 25/02/2023 22:17:57
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
BEGIN;
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (1, 'permission', '0001_initial', '2022-12-01 10:08:35.139175');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (2, 'user', '0001_initial', '2022-12-01 10:08:35.843469');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (3, 'operationlog', '0001_initial', '2022-12-01 10:08:36.012448');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (4, 'user', '0002_alter_userinfo_email', '2022-12-01 10:09:28.427797');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (5, 'user', '0003_auto_20221203_2124', '2022-12-03 21:26:08.665123');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (6, 'user', '0004_userinfo_is_superuser', '2022-12-03 21:26:08.838521');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (7, 'permission', '0002_roles_status', '2022-12-03 21:51:29.653231');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (8, 'permission', '0003_auto_20230223_1848', '2023-02-23 18:48:16.344962');
COMMIT;

-- ----------------------------
-- Table structure for lqz_dept
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dept`;
CREATE TABLE `lqz_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `sub_count` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `dept_sort` int(11) DEFAULT NULL,
  `pid_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `lqz_dept_pid_id_2013f194_fk_lqz_dept_id` (`pid_id`),
  CONSTRAINT `lqz_dept_pid_id_2013f194_fk_lqz_dept_id` FOREIGN KEY (`pid_id`) REFERENCES `lqz_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dept
-- ----------------------------
BEGIN;
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (1, '2022-12-04 22:50:01.000000', '2023-02-18 17:20:30.741096', 'admin', 'admin', 0, 3, '山东分部', 1, 1, NULL);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (2, '2022-12-04 22:50:51.000000', '2023-02-18 17:21:07.758148', 'admin', 'admin', 0, 0, '山东-开发', 1, 2, 1);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (3, '2022-12-04 22:51:33.000000', NULL, 'admin', 'admin', 0, 0, '山东-运维', 1, 3, 1);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (4, '2022-12-04 22:52:12.000000', NULL, 'admin', NULL, 0, 0, '山东-市场', 1, 4, 1);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (5, '2022-12-04 22:50:01.000000', '2022-12-04 22:54:28.000000', 'admin', 'admin', 0, 3, '上海分部', 1, 5, NULL);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (6, '2022-12-04 22:50:51.000000', NULL, 'admin', 'admin', 0, 0, '上海-运营', 1, 6, 5);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (7, '2022-12-04 22:51:33.000000', NULL, 'admin', 'admin', 0, 0, '上海-实施', 1, 7, 5);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (8, '2022-12-04 22:52:12.000000', NULL, 'admin', NULL, 0, 0, '上海-开发', 1, 8, 5);
COMMIT;

-- ----------------------------
-- Table structure for lqz_dict
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dict`;
CREATE TABLE `lqz_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dict
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dict_detail`;
CREATE TABLE `lqz_dict_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `dict_sort` int(11) DEFAULT NULL,
  `dict_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lqz_dict_detail_dict_id_bdc8808c_fk_lqz_dict_id` (`dict_id`),
  CONSTRAINT `lqz_dict_detail_dict_id_bdc8808c_fk_lqz_dict_id` FOREIGN KEY (`dict_id`) REFERENCES `lqz_dict` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dict_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_job
-- ----------------------------
DROP TABLE IF EXISTS `lqz_job`;
CREATE TABLE `lqz_job` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `job_sort` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_sort` (`job_sort`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_job
-- ----------------------------
BEGIN;
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (1, '2022-12-10 17:22:50.000000', NULL, 'admin', NULL, 0, '全栈开发', 1, 1);
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (2, '2022-12-10 17:23:12.000000', NULL, 'admin', NULL, 0, '人事专员', 1, 2);
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (3, '2022-12-10 17:23:32.000000', NULL, 'admin', NULL, 0, '产品经理', 1, 3);
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (4, '2022-12-10 17:23:51.000000', NULL, 'admin', NULL, 0, '软件测试', 1, 4);
COMMIT;

-- ----------------------------
-- Table structure for lqz_menu
-- ----------------------------
DROP TABLE IF EXISTS `lqz_menu`;
CREATE TABLE `lqz_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `sub_count` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `component` varchar(255) DEFAULT NULL,
  `menu_sort` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `i_frame` tinyint(1) NOT NULL,
  `cache` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `is_menu` tinyint(1) NOT NULL,
  `pid_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `name` (`name`),
  KEY `lqz_menu_pid_id_a5b538cc_fk_lqz_menu_id` (`pid_id`),
  CONSTRAINT `lqz_menu_pid_id_a5b538cc_fk_lqz_menu_id` FOREIGN KEY (`pid_id`) REFERENCES `lqz_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_menu
-- ----------------------------
BEGIN;
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (1, '2022-12-07 17:35:36.000000', NULL, 'admin', NULL, 0, 5, 0, '系统管理', 'SysManga', 'Layout', 1, 'el-icon-s-operation', '', 0, 0, 0, NULL, 1, NULL);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (2, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '用户管理', 'SysUser', 'admin/User', 2, 'el-icon-s-custom', '/admin/user', 0, 0, 0, 'user:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (3, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '角色管理', 'SysRole', 'admin/Role', 3, 'el-icon-rank', '/admin/role', 0, 0, 0, 'role:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (4, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '菜单管理', 'SysMenu', 'admin/Menu', 4, 'el-icon-menu', '/admin/menu', 0, 0, 0, 'menu:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (5, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '部门管理', 'SysDept', 'admin/Depts', 5, 'el-icon-menu', '/admin/dept', 0, 0, 0, 'depts:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (6, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '岗位管理', 'SysJob', 'admin/Job', 6, 'el-icon-menu', '/admin/job', 0, 0, 0, 'job:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (7, '2022-12-07 17:35:36.000000', NULL, 'admin', NULL, 0, 1, 0, '系统工具', 'SysTools', 'Layout', 7, 'el-icon-s-tools', '', 0, 0, 0, NULL, 1, NULL);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (8, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '字典管理', 'SysDict', 'admin/Dict', 8, 'el-icon-s-order', 'admin/dicts', 0, 0, 0, 'dict:list', 0, 7);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (9, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '用户新增', 'UserAdd', '', 8, '', '', 0, 0, 0, 'user:add', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (10, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '用户删除', 'UserDelete', '', 9, '', '', 0, 0, 0, 'user:delete', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (11, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '用户编辑', 'UserUpdate', '', 10, '', '', 0, 0, 0, 'user:update', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (12, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色新增', 'RoleAdd', '', 8, '', '', 0, 0, 0, 'role:add', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (13, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色删除', 'RoleDelete', '', 9, '', '', 0, 0, 0, 'role:delete', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (14, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色编辑', 'RoleUpdate', '', 10, '', '', 0, 0, 0, 'roleupdate', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (15, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单新增', 'MenuAdd', '', 8, '', '', 0, 0, 0, 'menu:add', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (16, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单删除', 'MenuDelete', '', 9, '', '', 0, 0, 0, 'menu:delete', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (17, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单编辑', 'MenuUpdate', '', 10, '', '', 0, 0, 0, 'menu:update', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (18, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门新增', 'DeptsAdd', '', 8, '', '', 0, 0, 0, 'depts:add', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (19, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门删除', 'DeptsDelete', '', 9, '', '', 0, 0, 0, 'depts:delete', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (20, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门编辑', 'DeptsUpdate', '', 10, '', '', 0, 0, 0, 'depts:update', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (21, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位新增', 'JobAdd', '', 8, '', '', 0, 0, 0, 'job:add', 0, 6);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (22, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位删除', 'JobDelete', '', 9, '', '', 0, 0, 0, 'job:delete', 0, 6);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (23, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位编辑', 'JobUpdate', '', 10, '', '', 0, 0, 0, 'job:update', 0, 6);
COMMIT;

-- ----------------------------
-- Table structure for lqz_online_user
-- ----------------------------
DROP TABLE IF EXISTS `lqz_online_user`;
CREATE TABLE `lqz_online_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `address` varchar(64) DEFAULT NULL,
  `browser` varchar(128) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lqz_online_user_user_id_248d38fd_fk_lqz_user_id` (`user_id`),
  CONSTRAINT `lqz_online_user_user_id_248d38fd_fk_lqz_user_id` FOREIGN KEY (`user_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_online_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_operation_logs
-- ----------------------------
DROP TABLE IF EXISTS `lqz_operation_logs`;
CREATE TABLE `lqz_operation_logs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `description` varchar(255) DEFAULT NULL,
  `modifier` varchar(32) DEFAULT NULL,
  `dept_belong_id` varchar(64) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `request_modular` varchar(64) DEFAULT NULL,
  `request_path` varchar(400) DEFAULT NULL,
  `request_body` longtext,
  `request_method` varchar(64) DEFAULT NULL,
  `request_msg` longtext,
  `request_ip` varchar(32) DEFAULT NULL,
  `request_browser` varchar(64) DEFAULT NULL,
  `response_code` varchar(32) DEFAULT NULL,
  `request_location` varchar(64) DEFAULT NULL,
  `request_os` varchar(64) DEFAULT NULL,
  `json_result` longtext,
  `status` tinyint(1) NOT NULL,
  `creator_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lqz_operation_logs_creator_id_8eb0d638` (`creator_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_operation_logs
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_role
-- ----------------------------
DROP TABLE IF EXISTS `lqz_role`;
CREATE TABLE `lqz_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `data_scope` varchar(32) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_role
-- ----------------------------
BEGIN;
INSERT INTO `lqz_role` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `level`, `description`, `data_scope`, `status`) VALUES (1, '2022-12-03 21:41:46.000000', '2023-02-25 22:15:21.682532', NULL, NULL, 0, '有用户角色权限角色', 13, '用户所有权限和角色所有权限', '全部', 1);
INSERT INTO `lqz_role` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `level`, `description`, `data_scope`, `status`) VALUES (2, NULL, '2023-02-25 21:51:56.560085', NULL, NULL, 0, '所有权限', 1, '所有权限', '自定义', 1);
COMMIT;

-- ----------------------------
-- Table structure for lqz_roles_depts
-- ----------------------------
DROP TABLE IF EXISTS `lqz_roles_depts`;
CREATE TABLE `lqz_roles_depts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roles_id` bigint(20) NOT NULL,
  `dept_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_roles_depts_roles_id_dept_id_ae9c29af_uniq` (`roles_id`,`dept_id`),
  KEY `lqz_roles_depts_dept_id_8e794f08_fk_lqz_dept_id` (`dept_id`),
  CONSTRAINT `lqz_roles_depts_dept_id_8e794f08_fk_lqz_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `lqz_dept` (`id`),
  CONSTRAINT `lqz_roles_depts_roles_id_ad60d386_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_roles_depts
-- ----------------------------
BEGIN;
INSERT INTO `lqz_roles_depts` (`id`, `roles_id`, `dept_id`) VALUES (9, 1, 3);
INSERT INTO `lqz_roles_depts` (`id`, `roles_id`, `dept_id`) VALUES (3, 2, 2);
INSERT INTO `lqz_roles_depts` (`id`, `roles_id`, `dept_id`) VALUES (4, 2, 3);
COMMIT;

-- ----------------------------
-- Table structure for lqz_roles_menus
-- ----------------------------
DROP TABLE IF EXISTS `lqz_roles_menus`;
CREATE TABLE `lqz_roles_menus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roles_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_roles_menus_roles_id_menu_id_909659bb_uniq` (`roles_id`,`menu_id`),
  KEY `lqz_roles_menus_menu_id_b0abd116_fk_lqz_menu_id` (`menu_id`),
  CONSTRAINT `lqz_roles_menus_menu_id_b0abd116_fk_lqz_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `lqz_menu` (`id`),
  CONSTRAINT `lqz_roles_menus_roles_id_34bb1dde_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=39 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_roles_menus
-- ----------------------------
BEGIN;
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (13, 1, 1);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (14, 1, 2);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (15, 1, 3);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (16, 1, 9);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (19, 1, 12);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (20, 1, 13);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (7, 2, 1);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (8, 2, 2);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (9, 2, 3);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (10, 2, 4);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (11, 2, 5);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (12, 2, 6);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (22, 2, 7);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (23, 2, 8);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (24, 2, 9);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (25, 2, 10);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (26, 2, 11);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (27, 2, 12);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (28, 2, 13);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (29, 2, 14);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (30, 2, 15);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (31, 2, 16);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (32, 2, 17);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (33, 2, 18);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (34, 2, 19);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (35, 2, 20);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (36, 2, 21);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (37, 2, 22);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (38, 2, 23);
COMMIT;

-- ----------------------------
-- Table structure for lqz_user
-- ----------------------------
DROP TABLE IF EXISTS `lqz_user`;
CREATE TABLE `lqz_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `password` varchar(128) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `email` varchar(254) DEFAULT NULL,
  `nick_name` varchar(32) DEFAULT NULL,
  `gender` varchar(16) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `pwd_reset_time` datetime(6) NOT NULL,
  `is_login` tinyint(1) NOT NULL,
  `dept_id` bigint(20) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `nick_name` (`nick_name`),
  UNIQUE KEY `phone` (`phone`),
  KEY `lqz_user_dept_id_25de3b75_fk_lqz_dept_id` (`dept_id`),
  CONSTRAINT `lqz_user_dept_id_25de3b75_fk_lqz_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `lqz_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_user
-- ----------------------------
BEGIN;
INSERT INTO `lqz_user` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `username`, `password`, `is_active`, `email`, `nick_name`, `gender`, `phone`, `enabled`, `pwd_reset_time`, `is_login`, `dept_id`, `avatar`, `is_superuser`) VALUES (1, NULL, '2023-02-25 21:55:09.753711', NULL, NULL, 0, 'lqz', 'pbkdf2_sha256$260000$Olag87RDbbCDx4KNP6yzIm$uIuhtVaPJUIUBIi+FmD1nOGJsFaflRPJvXzGmvH7cMs=', 1, '306@qq.com', 'lqz', '2', '13245388464', 1, '2022-12-01 10:10:44.000000', 1, 1, 'avatar/default.png', 1);
INSERT INTO `lqz_user` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `username`, `password`, `is_active`, `email`, `nick_name`, `gender`, `phone`, `enabled`, `pwd_reset_time`, `is_login`, `dept_id`, `avatar`, `is_superuser`) VALUES (2, '2022-12-03 17:48:44.857114', '2023-02-25 21:55:15.785963', NULL, NULL, 0, 'liuqingzheng', 'pbkdf2_sha256$260000$Olag87RDbbCDx4KNP6yzIm$uIuhtVaPJUIUBIi+FmD1nOGJsFaflRPJvXzGmvH7cMs=', 1, '44@163.com', '小妮', '2', '1888888888', 1, '2022-12-03 17:48:44.857321', 0, 2, 'avatar/default.png', 0);
INSERT INTO `lqz_user` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `username`, `password`, `is_active`, `email`, `nick_name`, `gender`, `phone`, `enabled`, `pwd_reset_time`, `is_login`, `dept_id`, `avatar`, `is_superuser`) VALUES (5, '2023-02-20 18:04:58.703661', '2023-02-20 18:05:21.623481', NULL, NULL, 0, 'lisixian', 'pbkdf2_sha256$260000$052gt68I1tNdrWOoEeLQnC$nJS+g8jEzkTfiI3FVz2ESY3NENFNpLDnVoPxDlSn5DU=', 1, '3@qq.com', '李思贤', '1', '1234555', 1, '2023-02-20 18:04:58.703755', 0, 1, 'avatar/default.png', 0);
COMMIT;

-- ----------------------------
-- Table structure for lqz_users_jobs
-- ----------------------------
DROP TABLE IF EXISTS `lqz_users_jobs`;
CREATE TABLE `lqz_users_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_users_jobs_userinfo_id_job_id_c7e9f3c7_uniq` (`userinfo_id`,`job_id`),
  KEY `lqz_users_jobs_job_id_9a99b906_fk_lqz_job_id` (`job_id`),
  CONSTRAINT `lqz_users_jobs_job_id_9a99b906_fk_lqz_job_id` FOREIGN KEY (`job_id`) REFERENCES `lqz_job` (`id`),
  CONSTRAINT `lqz_users_jobs_userinfo_id_7b41e7f5_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_users_jobs
-- ----------------------------
BEGIN;
INSERT INTO `lqz_users_jobs` (`id`, `userinfo_id`, `job_id`) VALUES (1, 1, 1);
INSERT INTO `lqz_users_jobs` (`id`, `userinfo_id`, `job_id`) VALUES (6, 5, 1);
INSERT INTO `lqz_users_jobs` (`id`, `userinfo_id`, `job_id`) VALUES (7, 5, 2);
COMMIT;

-- ----------------------------
-- Table structure for lqz_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `lqz_users_roles`;
CREATE TABLE `lqz_users_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_users_roles_userinfo_id_roles_id_a3790c91_uniq` (`userinfo_id`,`roles_id`),
  KEY `lqz_users_roles_roles_id_46de236c_fk_lqz_role_id` (`roles_id`),
  CONSTRAINT `lqz_users_roles_roles_id_46de236c_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`),
  CONSTRAINT `lqz_users_roles_userinfo_id_27e3c901_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_users_roles
-- ----------------------------
BEGIN;
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (8, 1, 2);
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (3, 2, 1);
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (9, 5, 1);
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (10, 5, 2);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
