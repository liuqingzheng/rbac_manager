/*
 Navicat Premium Data Transfer

 Source Server         : 本地rbac_manage用户
 Source Server Type    : MySQL
 Source Server Version : 50731
 Source Host           : 127.0.0.1:3306
 Source Schema         : rabc_manage

 Target Server Type    : MySQL
 Target Server Version : 50731
 File Encoding         : 65001

 Date: 21/09/2023 20:47:52
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for auth_group
-- ----------------------------
DROP TABLE IF EXISTS `auth_group`;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth_group
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for auth_group_permissions
-- ----------------------------
DROP TABLE IF EXISTS `auth_group_permissions`;
CREATE TABLE `auth_group_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`),
  CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth_group_permissions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for auth_permission
-- ----------------------------
DROP TABLE IF EXISTS `auth_permission`;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`),
  CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of auth_permission
-- ----------------------------
BEGIN;
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (1, 'Can add log entry', 1, 'add_logentry');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (2, 'Can change log entry', 1, 'change_logentry');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (3, 'Can delete log entry', 1, 'delete_logentry');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (4, 'Can view log entry', 1, 'view_logentry');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (5, 'Can add permission', 2, 'add_permission');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (6, 'Can change permission', 2, 'change_permission');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (7, 'Can delete permission', 2, 'delete_permission');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (8, 'Can view permission', 2, 'view_permission');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (9, 'Can add group', 3, 'add_group');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (10, 'Can change group', 3, 'change_group');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (11, 'Can delete group', 3, 'delete_group');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (12, 'Can view group', 3, 'view_group');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (13, 'Can add content type', 4, 'add_contenttype');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (14, 'Can change content type', 4, 'change_contenttype');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (15, 'Can delete content type', 4, 'delete_contenttype');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (16, 'Can view content type', 4, 'view_contenttype');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (17, 'Can add session', 5, 'add_session');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (18, 'Can change session', 5, 'change_session');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (19, 'Can delete session', 5, 'delete_session');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (20, 'Can view session', 5, 'view_session');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (21, 'Can add 用户表', 6, 'add_userinfo');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (22, 'Can change 用户表', 6, 'change_userinfo');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (23, 'Can delete 用户表', 6, 'delete_userinfo');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (24, 'Can view 用户表', 6, 'view_userinfo');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (25, 'Can add 在线用户的记录表', 7, 'add_onlineuser');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (26, 'Can change 在线用户的记录表', 7, 'change_onlineuser');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (27, 'Can delete 在线用户的记录表', 7, 'delete_onlineuser');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (28, 'Can view 在线用户的记录表', 7, 'view_onlineuser');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (29, 'Can add 字典详情表', 8, 'add_dictdetail');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (30, 'Can change 字典详情表', 8, 'change_dictdetail');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (31, 'Can delete 字典详情表', 8, 'delete_dictdetail');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (32, 'Can view 字典详情表', 8, 'view_dictdetail');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (33, 'Can add 数据字典', 9, 'add_dict');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (34, 'Can change 数据字典', 9, 'change_dict');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (35, 'Can delete 数据字典', 9, 'delete_dict');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (36, 'Can view 数据字典', 9, 'view_dict');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (37, 'Can add 角色表', 10, 'add_roles');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (38, 'Can change 角色表', 10, 'change_roles');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (39, 'Can delete 角色表', 10, 'delete_roles');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (40, 'Can view 角色表', 10, 'view_roles');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (41, 'Can add 岗位表', 11, 'add_job');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (42, 'Can change 岗位表', 11, 'change_job');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (43, 'Can delete 岗位表', 11, 'delete_job');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (44, 'Can view 岗位表', 11, 'view_job');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (45, 'Can add 部门表', 12, 'add_dept');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (46, 'Can change 部门表', 12, 'change_dept');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (47, 'Can delete 部门表', 12, 'delete_dept');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (48, 'Can view 部门表', 12, 'view_dept');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (49, 'Can add 菜单表', 13, 'add_menu');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (50, 'Can change 菜单表', 13, 'change_menu');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (51, 'Can delete 菜单表', 13, 'delete_menu');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (52, 'Can view 菜单表', 13, 'view_menu');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (53, 'Can add 网络设备表', 14, 'add_networkserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (54, 'Can change 网络设备表', 14, 'change_networkserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (55, 'Can delete 网络设备表', 14, 'delete_networkserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (56, 'Can view 网络设备表', 14, 'view_networkserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (57, 'Can add 其它设备表', 15, 'add_othermachine');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (58, 'Can change 其它设备表', 15, 'change_othermachine');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (59, 'Can delete 其它设备表', 15, 'delete_othermachine');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (60, 'Can view 其它设备表', 15, 'view_othermachine');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (61, 'Can add server group', 16, 'add_servergroup');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (62, 'Can change server group', 16, 'change_servergroup');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (63, 'Can delete server group', 16, 'delete_servergroup');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (64, 'Can view server group', 16, 'view_servergroup');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (65, 'Can add cloud server', 17, 'add_cloudserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (66, 'Can change cloud server', 17, 'change_cloudserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (67, 'Can delete cloud server', 17, 'delete_cloudserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (68, 'Can view cloud server', 17, 'view_cloudserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (69, 'Can add credential', 18, 'add_credential');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (70, 'Can change credential', 18, 'change_credential');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (71, 'Can delete credential', 18, 'delete_credential');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (72, 'Can view credential', 18, 'view_credential');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (73, 'Can add physical server', 19, 'add_physicalserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (74, 'Can change physical server', 19, 'change_physicalserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (75, 'Can delete physical server', 19, 'delete_physicalserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (76, 'Can view physical server', 19, 'view_physicalserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (77, 'Can add 虚拟设备', 20, 'add_virtualserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (78, 'Can change 虚拟设备', 20, 'change_virtualserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (79, 'Can delete 虚拟设备', 20, 'delete_virtualserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (80, 'Can view 虚拟设备', 20, 'view_virtualserver');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (81, 'Can add idc', 21, 'add_idc');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (82, 'Can change idc', 21, 'change_idc');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (83, 'Can delete idc', 21, 'delete_idc');
INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES (84, 'Can view idc', 21, 'view_idc');
COMMIT;

-- ----------------------------
-- Table structure for django_admin_log
-- ----------------------------
DROP TABLE IF EXISTS `django_admin_log`;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  KEY `django_admin_log_user_id_c564eba6_fk_lqz_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  CONSTRAINT `django_admin_log_user_id_c564eba6_fk_lqz_user_id` FOREIGN KEY (`user_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of django_admin_log
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for django_content_type
-- ----------------------------
DROP TABLE IF EXISTS `django_content_type`;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of django_content_type
-- ----------------------------
BEGIN;
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (1, 'admin', 'logentry');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (3, 'auth', 'group');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (2, 'auth', 'permission');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (17, 'cmdb', 'cloudserver');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (18, 'cmdb', 'credential');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (21, 'cmdb', 'idc');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (14, 'cmdb', 'networkserver');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (15, 'cmdb', 'othermachine');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (19, 'cmdb', 'physicalserver');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (16, 'cmdb', 'servergroup');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (20, 'cmdb', 'virtualserver');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (4, 'contenttypes', 'contenttype');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (12, 'permission', 'dept');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (9, 'permission', 'dict');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (8, 'permission', 'dictdetail');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (11, 'permission', 'job');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (13, 'permission', 'menu');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (10, 'permission', 'roles');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (5, 'sessions', 'session');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (7, 'user', 'onlineuser');
INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES (6, 'user', 'userinfo');
COMMIT;

-- ----------------------------
-- Table structure for django_migrations
-- ----------------------------
DROP TABLE IF EXISTS `django_migrations`;
CREATE TABLE `django_migrations` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of django_migrations
-- ----------------------------
BEGIN;
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (1, 'contenttypes', '0001_initial', '2022-12-11 10:28:11.547704');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (2, 'contenttypes', '0002_remove_content_type_name', '2022-12-11 10:28:11.692493');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (3, 'auth', '0001_initial', '2022-12-11 10:28:12.142332');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (4, 'auth', '0002_alter_permission_name_max_length', '2022-12-11 10:28:12.223938');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (5, 'auth', '0003_alter_user_email_max_length', '2022-12-11 10:28:12.257126');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (6, 'auth', '0004_alter_user_username_opts', '2022-12-11 10:28:12.299511');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (7, 'auth', '0005_alter_user_last_login_null', '2022-12-11 10:28:12.331439');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (8, 'auth', '0006_require_contenttypes_0002', '2022-12-11 10:28:12.365239');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (9, 'auth', '0007_alter_validators_add_error_messages', '2022-12-11 10:28:12.407143');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (10, 'auth', '0008_alter_user_username_max_length', '2022-12-11 10:28:12.448415');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (11, 'auth', '0009_alter_user_last_name_max_length', '2022-12-11 10:28:12.486555');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (12, 'auth', '0010_alter_group_name_max_length', '2022-12-11 10:28:12.592313');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (13, 'auth', '0011_update_proxy_permissions', '2022-12-11 10:28:12.634806');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (14, 'auth', '0012_alter_user_first_name_max_length', '2022-12-11 10:28:12.664395');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (15, 'user', '0001_initial', '2022-12-11 10:28:13.215908');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (16, 'admin', '0001_initial', '2022-12-11 10:28:13.420222');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (17, 'admin', '0002_logentry_remove_auto_add', '2022-12-11 10:28:13.470400');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (18, 'admin', '0003_logentry_add_action_flag_choices', '2022-12-11 10:28:13.512247');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (19, 'sessions', '0001_initial', '2022-12-11 10:28:13.649418');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (20, 'permission', '0001_initial', '2022-12-18 03:08:38.697447');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (21, 'user', '0002_auto_20221218_0308', '2022-12-18 03:08:39.381768');
INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES (22, 'cmdb', '0001_initial', '2023-03-12 11:25:21.466394');
COMMIT;

-- ----------------------------
-- Table structure for django_session
-- ----------------------------
DROP TABLE IF EXISTS `django_session`;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_expire_date_a5c62663` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of django_session
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_cloud_server
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_cloud_server`;
CREATE TABLE `lqz_cmdb_cloud_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `hostname` varchar(30) NOT NULL,
  `ssh_ip` char(39) NOT NULL,
  `ssh_port` int(11) NOT NULL,
  `note` longtext,
  `machine_type` varchar(30) NOT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `public_ip` json DEFAULT NULL,
  `private_ip` json DEFAULT NULL,
  `cpu_num` varchar(10) DEFAULT NULL,
  `memory` varchar(30) DEFAULT NULL,
  `disk` json DEFAULT NULL,
  `network` varchar(200) DEFAULT NULL,
  `put_shelves_date` date DEFAULT NULL,
  `off_shelves_date` date DEFAULT NULL,
  `expire_datetime` datetime(6) DEFAULT NULL,
  `is_verified` varchar(10) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `credential_id` bigint(20) DEFAULT NULL,
  `idc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `lqz_cmdb_cloud_serve_credential_id_093f4e6d_fk_lqz_cmdb_` (`credential_id`),
  KEY `lqz_cmdb_cloud_server_idc_id_70b8706c_fk_lqz_cmdb_idc_id` (`idc_id`),
  CONSTRAINT `lqz_cmdb_cloud_serve_credential_id_093f4e6d_fk_lqz_cmdb_` FOREIGN KEY (`credential_id`) REFERENCES `lqz_cmdb_credential` (`id`),
  CONSTRAINT `lqz_cmdb_cloud_server_idc_id_70b8706c_fk_lqz_cmdb_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `lqz_cmdb_idc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_cloud_server
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_cloud_server` (`id`, `name`, `hostname`, `ssh_ip`, `ssh_port`, `note`, `machine_type`, `os_version`, `public_ip`, `private_ip`, `cpu_num`, `memory`, `disk`, `network`, `put_shelves_date`, `off_shelves_date`, `expire_datetime`, `is_verified`, `update_time`, `create_time`, `credential_id`, `idc_id`) VALUES (19, 'iZbp1ag27anavys9tg06aaZ', 'i-bp1ag27anavys9tg06aa', '47.98.210.110', 22, NULL, 'linux', 'CentOS  7.9 64位', '[\"47.98.210.110\"]', '[\"172.23.28.13\"]', '1核', '1.0G', '[{\"size\": \"20G\", \"type\": \"cloud_efficiency\", \"device\": \"/dev/xvda\"}]', '5M', '2023-03-19', NULL, '2099-12-31 15:59:00.000000', 'unverified', '2023-03-19 17:49:47.126463', '2023-03-19 17:49:47.126477', NULL, 13);
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_cloud_server_server_group
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_cloud_server_server_group`;
CREATE TABLE `lqz_cmdb_cloud_server_server_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `cloudserver_id` bigint(20) NOT NULL,
  `servergroup_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_cmdb_cloud_server_se_cloudserver_id_servergro_91d89c09_uniq` (`cloudserver_id`,`servergroup_id`),
  KEY `lqz_cmdb_cloud_serve_servergroup_id_c36b79da_fk_lqz_cmdb_` (`servergroup_id`),
  CONSTRAINT `lqz_cmdb_cloud_serve_cloudserver_id_b690790b_fk_lqz_cmdb_` FOREIGN KEY (`cloudserver_id`) REFERENCES `lqz_cmdb_cloud_server` (`id`),
  CONSTRAINT `lqz_cmdb_cloud_serve_servergroup_id_c36b79da_fk_lqz_cmdb_` FOREIGN KEY (`servergroup_id`) REFERENCES `lqz_cmdb_server_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_cloud_server_server_group
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_cloud_server_server_group` (`id`, `cloudserver_id`, `servergroup_id`) VALUES (12, 19, 3);
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_credential
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_credential`;
CREATE TABLE `lqz_cmdb_credential` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `username` varchar(20) NOT NULL,
  `auth_mode` int(11) NOT NULL,
  `password` varchar(50) NOT NULL,
  `private_key` longtext NOT NULL,
  `note` longtext NOT NULL,
  `create_time` datetime(6) NOT NULL,
  `update_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_credential
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_credential` (`id`, `name`, `username`, `auth_mode`, `password`, `private_key`, `note`, `create_time`, `update_time`) VALUES (1, 'Linux通用机器', 'root', 1, 'lqz123', '', '采集的Linux机器的用户名和密码', '2023-03-12 14:28:36.848941', '2023-03-12 14:28:36.849056');
INSERT INTO `lqz_cmdb_credential` (`id`, `name`, `username`, `auth_mode`, `password`, `private_key`, `note`, `create_time`, `update_time`) VALUES (2, 'Windows通用机器', 'lqz', 1, 'lqz123', '', '采集win机器的通用用户名和密码', '2023-03-12 14:29:14.933840', '2023-03-12 14:29:14.933868');
INSERT INTO `lqz_cmdb_credential` (`id`, `name`, `username`, `auth_mode`, `password`, `private_key`, `note`, `create_time`, `update_time`) VALUES (5, '阿里云机器', 'root', 1, 'LiuQingzheng12#', '', '阿里云root用户密码', '2023-03-19 10:45:30.925352', '2023-03-19 10:45:30.925410');
INSERT INTO `lqz_cmdb_credential` (`id`, `name`, `username`, `auth_mode`, `password`, `private_key`, `note`, `create_time`, `update_time`) VALUES (6, '腾讯云', 'root', 1, 'LiuQingzheng123', '', '腾讯云机器', '2023-03-19 10:45:51.973538', '2023-03-19 16:49:05.647034');
INSERT INTO `lqz_cmdb_credential` (`id`, `name`, `username`, `auth_mode`, `password`, `private_key`, `note`, `create_time`, `update_time`) VALUES (7, 'Exsi虚拟机', 'root', 1, 'LiuQingzheng123', '', '虚拟机用户名密码', '2023-03-19 10:46:30.558024', '2023-03-19 14:50:18.051449');
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_idc
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_idc`;
CREATE TABLE `lqz_cmdb_idc` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `city` varchar(20) NOT NULL,
  `provider` varchar(30) NOT NULL,
  `note` longtext,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_idc
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_idc` (`id`, `name`, `city`, `provider`, `note`, `create_time`) VALUES (1, '北京机房01号', '北京', '电信', '北京电信01机房', '2023-03-12 12:09:34.348119');
INSERT INTO `lqz_cmdb_idc` (`id`, `name`, `city`, `provider`, `note`, `create_time`) VALUES (10, '上海机房', '杭州', '联通', '备注一下', '2023-03-19 10:42:07.207263');
INSERT INTO `lqz_cmdb_idc` (`id`, `name`, `city`, `provider`, `note`, `create_time`) VALUES (13, '华东 1 可用区 H', '华东1（杭州）', '阿里云', NULL, '2023-03-19 17:20:31.014748');
INSERT INTO `lqz_cmdb_idc` (`id`, `name`, `city`, `provider`, `note`, `create_time`) VALUES (14, '上海二区', '华东地区(上海)', '腾讯云', NULL, '2023-03-19 17:21:56.726118');
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_network_server
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_network_server`;
CREATE TABLE `lqz_cmdb_network_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `host_ip` varchar(40) NOT NULL,
  `host_name` varchar(10) NOT NULL,
  `sn_key` varchar(256) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_network_server
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_network_server` (`id`, `host_ip`, `host_name`, `sn_key`) VALUES (1, '10.0.0.202', '交换机001', 'asdfasd33343aweasqrwsddsss');
INSERT INTO `lqz_cmdb_network_server` (`id`, `host_ip`, `host_name`, `sn_key`) VALUES (2, '10.0.0.205', '路由器', 'ssasd3300asdfads');
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_other_machine
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_other_machine`;
CREATE TABLE `lqz_cmdb_other_machine` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `ip` varchar(40) NOT NULL,
  `sn_key` varchar(256) NOT NULL,
  `machine_name` varchar(20) NOT NULL,
  `note` longtext NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_other_machine
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_other_machine` (`id`, `ip`, `sn_key`, `machine_name`, `note`) VALUES (1, '10.0.0.206', 'asdfasdfas9940223fffssd', '未知设备01', '未知的设备，暂存');
INSERT INTO `lqz_cmdb_other_machine` (`id`, `ip`, `sn_key`, `machine_name`, `note`) VALUES (2, '100.0.0.207', 'asd453sss', '未知设备02', '');
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_physical_server
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_physical_server`;
CREATE TABLE `lqz_cmdb_physical_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `hostname` varchar(30) NOT NULL,
  `ssh_ip` char(39) NOT NULL,
  `ssh_port` int(11) NOT NULL,
  `note` longtext,
  `machine_type` varchar(30) NOT NULL,
  `asset_code` varchar(50) DEFAULT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `public_ip` json DEFAULT NULL,
  `private_ip` json DEFAULT NULL,
  `cpu_num` varchar(10) DEFAULT NULL,
  `cpu_model` varchar(100) DEFAULT NULL,
  `memory` varchar(30) DEFAULT NULL,
  `disk` json DEFAULT NULL,
  `network` varchar(200) DEFAULT NULL,
  `put_shelves_date` date DEFAULT NULL,
  `off_shelves_date` date DEFAULT NULL,
  `is_verified` varchar(10) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `credential_id` bigint(20) DEFAULT NULL,
  `idc_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `lqz_cmdb_physical_se_credential_id_91de17ba_fk_lqz_cmdb_` (`credential_id`),
  KEY `lqz_cmdb_physical_server_idc_id_01b347fe_fk_lqz_cmdb_idc_id` (`idc_id`),
  CONSTRAINT `lqz_cmdb_physical_se_credential_id_91de17ba_fk_lqz_cmdb_` FOREIGN KEY (`credential_id`) REFERENCES `lqz_cmdb_credential` (`id`),
  CONSTRAINT `lqz_cmdb_physical_server_idc_id_01b347fe_fk_lqz_cmdb_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `lqz_cmdb_idc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_physical_server
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_physical_server` (`id`, `name`, `hostname`, `ssh_ip`, `ssh_port`, `note`, `machine_type`, `asset_code`, `os_version`, `public_ip`, `private_ip`, `cpu_num`, `cpu_model`, `memory`, `disk`, `network`, `put_shelves_date`, `off_shelves_date`, `is_verified`, `update_time`, `create_time`, `credential_id`, `idc_id`) VALUES (1, 'Linux实体机测试', 'hostname-lqz', '10.0.0.102', 22, NULL, 'linux', 'asdfasasf999332asdfasdf', 'CentOS Linux release 7.6.1810 (Core)', '[\"222.65.23.38\"]', '[\"10.0.0.102\"]', '2核', 'Intel(R) Core(TM) i7-8850H CPU @ 2.60GHz', '1.9G', '[{\"size\": \"64G\", \"type\": \"HDD\", \"device\": \"/dev/sda\"}]', '', '2023-03-11', NULL, 'verified', '2023-03-19 10:47:15.707623', '2023-03-12 16:43:07.498708', 1, 1);
INSERT INTO `lqz_cmdb_physical_server` (`id`, `name`, `hostname`, `ssh_ip`, `ssh_port`, `note`, `machine_type`, `asset_code`, `os_version`, `public_ip`, `private_ip`, `cpu_num`, `cpu_model`, `memory`, `disk`, `network`, `put_shelves_date`, `off_shelves_date`, `is_verified`, `update_time`, `create_time`, `credential_id`, `idc_id`) VALUES (2, 'windows实体机测试', '901A', '10.0.0.3', 22, NULL, 'windows', 'asdfasfas880222asdfasd', 'Microsoft Windows 10 专业版', '[\"222.65.23.38\"]', '[\"10.0.0.3\"]', '4核', 'Intel(R) Core(TM) i7-8850H CPU @ 2.60GHz', '2GB', '[{\"size\": \"127GB\", \"type\": \"None\", \"device\": \"C\"}]', '100000M', NULL, NULL, 'verified', '2023-03-19 11:44:14.439223', '2023-03-12 16:43:52.141220', 2, 1);
INSERT INTO `lqz_cmdb_physical_server` (`id`, `name`, `hostname`, `ssh_ip`, `ssh_port`, `note`, `machine_type`, `asset_code`, `os_version`, `public_ip`, `private_ip`, `cpu_num`, `cpu_model`, `memory`, `disk`, `network`, `put_shelves_date`, `off_shelves_date`, `is_verified`, `update_time`, `create_time`, `credential_id`, `idc_id`) VALUES (4, 'esxi7.0服务器', 'localhost.localdomain', '192.168.176.130', 443, NULL, 'vmware', 'eeee00asdfasd', 'VMware ESXi 7.0.0 build-16324942', '[\"192.168.176.130\"]', '[\"192.168.176.130\"]', '4核', 'Intel(R) Core(TM) i7-8850H CPU @ 2.60GHz', '4.00G', '[{\"size\": \"13GB\", \"type\": \"VMFS\", \"device\": \"datastore1\"}]', '\'vim.Network:HaNetwork-VM Network\'', NULL, NULL, 'verified', '2023-03-19 14:49:58.658929', '2023-03-19 14:49:58.659147', 7, 10);
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_physical_server_server_group
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_physical_server_server_group`;
CREATE TABLE `lqz_cmdb_physical_server_server_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `physicalserver_id` bigint(20) NOT NULL,
  `servergroup_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_cmdb_physical_server_physicalserver_id_server_fb48d23b_uniq` (`physicalserver_id`,`servergroup_id`),
  KEY `lqz_cmdb_physical_se_servergroup_id_7717ed2e_fk_lqz_cmdb_` (`servergroup_id`),
  CONSTRAINT `lqz_cmdb_physical_se_physicalserver_id_61739e7e_fk_lqz_cmdb_` FOREIGN KEY (`physicalserver_id`) REFERENCES `lqz_cmdb_physical_server` (`id`),
  CONSTRAINT `lqz_cmdb_physical_se_servergroup_id_7717ed2e_fk_lqz_cmdb_` FOREIGN KEY (`servergroup_id`) REFERENCES `lqz_cmdb_server_group` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_physical_server_server_group
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_physical_server_server_group` (`id`, `physicalserver_id`, `servergroup_id`) VALUES (4, 1, 2);
INSERT INTO `lqz_cmdb_physical_server_server_group` (`id`, `physicalserver_id`, `servergroup_id`) VALUES (5, 2, 1);
INSERT INTO `lqz_cmdb_physical_server_server_group` (`id`, `physicalserver_id`, `servergroup_id`) VALUES (6, 4, 2);
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_server_group
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_server_group`;
CREATE TABLE `lqz_cmdb_server_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `note` longtext,
  `create_time` datetime(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_server_group
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_server_group` (`id`, `name`, `note`, `create_time`) VALUES (1, '测试组', '这是测试2222', '2023-03-12 14:10:15.667572');
INSERT INTO `lqz_cmdb_server_group` (`id`, `name`, `note`, `create_time`) VALUES (2, '开发组', '这是开发', '2023-03-12 14:10:29.759128');
INSERT INTO `lqz_cmdb_server_group` (`id`, `name`, `note`, `create_time`) VALUES (3, '阿里云机器', '所有阿里云机器放在这个组就ok', '2023-03-19 10:42:43.611477');
INSERT INTO `lqz_cmdb_server_group` (`id`, `name`, `note`, `create_time`) VALUES (4, '腾讯云机器', '所有腾讯云机器放在这个组', '2023-03-19 10:43:02.537605');
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_virtual_server
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_virtual_server`;
CREATE TABLE `lqz_cmdb_virtual_server` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(30) NOT NULL,
  `hostname` varchar(30) NOT NULL,
  `ssh_ip` char(39) NOT NULL,
  `ssh_port` int(11) NOT NULL,
  `note` longtext,
  `machine_type` varchar(30) DEFAULT NULL,
  `os_version` varchar(50) DEFAULT NULL,
  `public_ip` json DEFAULT NULL,
  `private_ip` json DEFAULT NULL,
  `cpu_num` varchar(10) DEFAULT NULL,
  `cpu_model` varchar(100) DEFAULT NULL,
  `memory` varchar(30) DEFAULT NULL,
  `disk` json DEFAULT NULL,
  `network` varchar(200) DEFAULT NULL,
  `put_shelves_date` date DEFAULT NULL,
  `off_shelves_date` date DEFAULT NULL,
  `is_verified` varchar(10) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `credential_id` bigint(20) DEFAULT NULL,
  `idc_id` bigint(20) NOT NULL,
  `vm_host_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `hostname` (`hostname`),
  KEY `lqz_cmdb_virtual_ser_credential_id_6ee5403b_fk_lqz_cmdb_` (`credential_id`),
  KEY `lqz_cmdb_virtual_server_idc_id_aaaa0b9c_fk_lqz_cmdb_idc_id` (`idc_id`),
  KEY `lqz_cmdb_virtual_ser_vm_host_id_f0eaf03b_fk_lqz_cmdb_` (`vm_host_id`),
  CONSTRAINT `lqz_cmdb_virtual_ser_credential_id_6ee5403b_fk_lqz_cmdb_` FOREIGN KEY (`credential_id`) REFERENCES `lqz_cmdb_credential` (`id`),
  CONSTRAINT `lqz_cmdb_virtual_ser_vm_host_id_f0eaf03b_fk_lqz_cmdb_` FOREIGN KEY (`vm_host_id`) REFERENCES `lqz_cmdb_physical_server` (`id`),
  CONSTRAINT `lqz_cmdb_virtual_server_idc_id_aaaa0b9c_fk_lqz_cmdb_idc_id` FOREIGN KEY (`idc_id`) REFERENCES `lqz_cmdb_idc` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_virtual_server
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_virtual_server` (`id`, `name`, `hostname`, `ssh_ip`, `ssh_port`, `note`, `machine_type`, `os_version`, `public_ip`, `private_ip`, `cpu_num`, `cpu_model`, `memory`, `disk`, `network`, `put_shelves_date`, `off_shelves_date`, `is_verified`, `update_time`, `create_time`, `credential_id`, `idc_id`, `vm_host_id`) VALUES (2, '虚拟机01', 'lqz-hostmane2', '192.168.170.133', 22, NULL, 'linux', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'unverified', '2023-03-19 15:15:36.476102', '2023-03-19 15:15:17.951378', 1, 1, 4);
COMMIT;

-- ----------------------------
-- Table structure for lqz_cmdb_virtual_server_server_group
-- ----------------------------
DROP TABLE IF EXISTS `lqz_cmdb_virtual_server_server_group`;
CREATE TABLE `lqz_cmdb_virtual_server_server_group` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `virtualserver_id` bigint(20) NOT NULL,
  `servergroup_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_cmdb_virtual_server__virtualserver_id_serverg_fb33672c_uniq` (`virtualserver_id`,`servergroup_id`),
  KEY `lqz_cmdb_virtual_ser_servergroup_id_eb0a3083_fk_lqz_cmdb_` (`servergroup_id`),
  CONSTRAINT `lqz_cmdb_virtual_ser_servergroup_id_eb0a3083_fk_lqz_cmdb_` FOREIGN KEY (`servergroup_id`) REFERENCES `lqz_cmdb_server_group` (`id`),
  CONSTRAINT `lqz_cmdb_virtual_ser_virtualserver_id_f3933a1f_fk_lqz_cmdb_` FOREIGN KEY (`virtualserver_id`) REFERENCES `lqz_cmdb_virtual_server` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_cmdb_virtual_server_server_group
-- ----------------------------
BEGIN;
INSERT INTO `lqz_cmdb_virtual_server_server_group` (`id`, `virtualserver_id`, `servergroup_id`) VALUES (2, 2, 1);
COMMIT;

-- ----------------------------
-- Table structure for lqz_dept
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dept`;
CREATE TABLE `lqz_dept` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `sub_count` int(11) DEFAULT NULL,
  `name` varchar(64) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `dept_sort` int(11) DEFAULT NULL,
  `pid_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `lqz_dept_pid_id_2013f194_fk_lqz_dept_id` (`pid_id`),
  CONSTRAINT `lqz_dept_pid_id_2013f194_fk_lqz_dept_id` FOREIGN KEY (`pid_id`) REFERENCES `lqz_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dept
-- ----------------------------
BEGIN;
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (1, '2023-02-26 18:00:19.000000', '2023-02-26 18:00:22.000000', 'admin', NULL, 0, 2, '山东分公司', 0, 1, NULL);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (2, '2023-02-26 18:00:49.000000', '2023-02-26 18:00:51.000000', NULL, NULL, 0, 0, '山东-开发', 0, 2, 1);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (3, '2023-02-26 18:01:11.000000', '2023-02-26 18:01:13.000000', NULL, NULL, 0, 0, '山东-运维', 0, 3, 1);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (4, '2023-02-26 18:01:37.000000', '2023-02-26 18:01:39.000000', NULL, NULL, 0, 1, '上海分公司', 0, 4, NULL);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (5, '2023-02-26 18:01:57.000000', '2023-02-26 18:01:59.000000', NULL, NULL, 0, 0, '上海-人事', 0, 7, 4);
INSERT INTO `lqz_dept` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `name`, `enabled`, `dept_sort`, `pid_id`) VALUES (6, '2023-03-05 14:33:47.243118', '2023-03-05 14:33:47.243179', 'admin', NULL, 0, NULL, '深圳分公司', 1, 6, NULL);
COMMIT;

-- ----------------------------
-- Table structure for lqz_dict
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dict`;
CREATE TABLE `lqz_dict` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dict
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_dict_detail
-- ----------------------------
DROP TABLE IF EXISTS `lqz_dict_detail`;
CREATE TABLE `lqz_dict_detail` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `label` varchar(64) DEFAULT NULL,
  `value` varchar(64) DEFAULT NULL,
  `dict_sort` int(11) DEFAULT NULL,
  `dict_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lqz_dict_detail_dict_id_bdc8808c_fk_lqz_dict_id` (`dict_id`),
  CONSTRAINT `lqz_dict_detail_dict_id_bdc8808c_fk_lqz_dict_id` FOREIGN KEY (`dict_id`) REFERENCES `lqz_dict` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_dict_detail
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_job
-- ----------------------------
DROP TABLE IF EXISTS `lqz_job`;
CREATE TABLE `lqz_job` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL,
  `job_sort` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `job_sort` (`job_sort`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_job
-- ----------------------------
BEGIN;
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (1, '2023-02-26 17:50:53.000000', '2023-02-26 17:50:55.000000', 'admin', 'admin', 0, '人事专员', 1, 1);
INSERT INTO `lqz_job` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `enabled`, `job_sort`) VALUES (2, '2023-02-26 17:51:18.000000', '2023-02-26 17:51:21.000000', 'admin', 'admin', 0, '运维人员', 0, 2);
COMMIT;

-- ----------------------------
-- Table structure for lqz_menu
-- ----------------------------
DROP TABLE IF EXISTS `lqz_menu`;
CREATE TABLE `lqz_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `sub_count` int(11) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  `title` varchar(32) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `component` varchar(255) DEFAULT NULL,
  `menu_sort` int(11) DEFAULT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `path` varchar(255) DEFAULT NULL,
  `i_frame` tinyint(1) NOT NULL,
  `cache` tinyint(1) NOT NULL,
  `hidden` tinyint(1) NOT NULL,
  `permission` varchar(255) DEFAULT NULL,
  `is_menu` tinyint(1) NOT NULL,
  `pid_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `title` (`title`),
  UNIQUE KEY `name` (`name`),
  KEY `lqz_menu_pid_id_a5b538cc_fk_lqz_menu_id` (`pid_id`),
  CONSTRAINT `lqz_menu_pid_id_a5b538cc_fk_lqz_menu_id` FOREIGN KEY (`pid_id`) REFERENCES `lqz_menu` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_menu
-- ----------------------------
BEGIN;
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (1, '2022-12-07 17:35:36.000000', '2023-03-05 16:11:08.903279', 'admin', NULL, 0, 5, 0, '系统管理', 'SysManga', 'Layout', 1, 'el-icon-s-operation', '', 0, 0, 0, NULL, 1, NULL);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (2, '2022-12-07 17:39:36.000000', '2023-03-05 16:12:40.156247', 'dmin', NULL, 0, 0, 1, '用户管理', 'SysUser', 'admin/User', 2, 'el-icon-s-custom', '/admin/user', 0, 0, 0, 'user:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (3, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '角色管理', 'SysRole', 'admin/Role', 3, 'el-icon-rank', '/admin/role', 0, 0, 0, 'role:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (4, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '菜单管理', 'SysMenu', 'admin/Menu', 4, 'el-icon-menu', '/admin/menu', 0, 0, 0, 'menu:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (5, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '部门管理', 'SysDept', 'admin/Dept', 5, 'el-icon-menu', '/admin/dept', 0, 0, 0, 'depts:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (6, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '岗位管理', 'SysJob', 'admin/Job', 6, 'el-icon-menu', '/admin/job', 0, 0, 0, 'job:list', 0, 1);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (7, '2022-12-07 17:35:36.000000', NULL, 'admin', NULL, 0, 1, 0, '系统工具', 'SysTools', 'Layout', 7, 'el-icon-s-tools', '', 0, 0, 0, NULL, 1, NULL);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (8, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '字典管理', 'SysDict', 'admin/Dict', 8, 'el-icon-s-order', 'admin/dicts', 0, 0, 0, 'dict:list', 0, 7);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (9, '2022-12-07 17:39:36.000000', '2023-03-05 16:13:01.053138', 'dmin', NULL, 0, 0, 2, '用户新增', 'UserAdd', NULL, 8, '', '', 0, 0, 0, 'user:add', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (10, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '用户删除', 'UserDelete', '', 9, '', '', 0, 0, 0, 'user:delete', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (11, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '用户编辑', 'UserUpdate', '', 10, '', '', 0, 0, 0, 'user:update', 0, 2);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (12, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色新增', 'RoleAdd', '', 8, '', '', 0, 0, 0, 'role:add', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (13, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色删除', 'RoleDelete', '', 9, '', '', 0, 0, 0, 'role:delete', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (14, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '角色编辑', 'RoleUpdate', '', 10, '', '', 0, 0, 0, 'role:update', 0, 3);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (15, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单新增', 'MenuAdd', '', 8, '', '', 0, 0, 0, 'menu:add', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (16, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单删除', 'MenuDelete', '', 9, '', '', 0, 0, 0, 'menu:delete', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (17, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '菜单编辑', 'MenuUpdate', '', 10, '', '', 0, 0, 0, 'menu:update', 0, 4);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (18, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门新增', 'DeptsAdd', '', 8, '', '', 0, 0, 0, 'depts:add', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (19, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门删除', 'DeptsDelete', '', 9, '', '', 0, 0, 0, 'depts:delete', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (20, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '部门编辑', 'DeptsUpdate', '', 10, '', '', 0, 0, 0, 'depts:update', 0, 5);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (21, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位新增', 'JobAdd', '', 8, '', '', 0, 0, 0, 'job:add', 0, 6);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (22, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位删除', 'JobDelete', '', 9, '', '', 0, 0, 0, 'job:delete', 0, 6);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (23, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 2, '岗位编辑', 'JobUpdate', '', 10, '', '', 0, 0, 0, 'job:update', 0, 6);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (24, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 0, 'CMDB', 'CMDB', 'Layout', 16, 'el-icon-menu', '', 0, 0, 0, '', 1, NULL);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (34, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, 'IDC机房管理', 'IDC', 'cmdb/IDC', 2, 'el-icon-s-custom', '/cmdb/IDC', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (36, '2022-12-07 17:39:36.000000', '2023-03-12 11:32:26.125228', 'dmin', NULL, 0, 0, 1, '主机分组管理', 'ServerGroup', 'cmdb/ServerGroup', 3, 'el-icon-rank', '/cmdb/ServerGroup', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (37, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '云主机管理', 'CloudServer', 'cmdb/CloudServer', 4, 'el-icon-menu', '/cmdb/CloudServer', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (38, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '物理机管理', 'PhysicalServer', 'cmdb/PhysicalServer', 5, 'el-icon-menu', '/cmdb/PhysicalServer', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (39, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '虚拟机管理', 'VirtualServer', 'cmdb/VirtualServer', 6, 'el-icon-menu', '/cmdb/VirtualServer', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (42, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '网络设备', 'NetWorkServer', 'cmdb/NetWorkServer', 6, 'el-icon-menu', '/cmdb/NetWorkServer', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (43, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '其他设备', 'OtherMachine', 'cmdb/OtherMachine', 6, 'el-icon-menu', '/cmdb/OtherMachine', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (44, '2022-12-07 17:39:36.000000', NULL, 'dmin', NULL, 0, 0, 1, '凭据管理', 'Credential', 'cmdb/Credential', 6, 'el-icon-menu', '/cmdb/Credential', 0, 0, 0, '', 0, 24);
INSERT INTO `lqz_menu` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `sub_count`, `type`, `title`, `name`, `component`, `menu_sort`, `icon`, `path`, `i_frame`, `cache`, `hidden`, `permission`, `is_menu`, `pid_id`) VALUES (45, '2023-03-19 17:36:44.984938', '2023-03-19 17:36:44.985064', NULL, NULL, 0, NULL, 2, '新增', '新增机房', NULL, 33, NULL, NULL, 0, 0, 0, 'idc:add', 0, 34);
COMMIT;

-- ----------------------------
-- Table structure for lqz_online_user
-- ----------------------------
DROP TABLE IF EXISTS `lqz_online_user`;
CREATE TABLE `lqz_online_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `address` varchar(64) DEFAULT NULL,
  `browser` varchar(128) DEFAULT NULL,
  `ip` varchar(64) DEFAULT NULL,
  `key` varchar(255) DEFAULT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `lqz_online_user_user_id_248d38fd_fk_lqz_user_id` (`user_id`),
  CONSTRAINT `lqz_online_user_user_id_248d38fd_fk_lqz_user_id` FOREIGN KEY (`user_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_online_user
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_role
-- ----------------------------
DROP TABLE IF EXISTS `lqz_role`;
CREATE TABLE `lqz_role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `name` varchar(32) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  `data_scope` varchar(32) DEFAULT NULL,
  `status` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_role
-- ----------------------------
BEGIN;
INSERT INTO `lqz_role` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `level`, `description`, `data_scope`, `status`) VALUES (1, '2023-02-26 18:11:29.000000', '2023-03-05 16:17:21.475938', NULL, NULL, 0, '超级管理员', 1, '超级管理员', '全部', 0);
INSERT INTO `lqz_role` (`id`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `name`, `level`, `description`, `data_scope`, `status`) VALUES (4, '2023-03-05 16:15:37.391353', '2023-03-19 17:38:39.123628', NULL, NULL, 0, '普通管理员', 3, '普通管理员', '全部', 1);
COMMIT;

-- ----------------------------
-- Table structure for lqz_roles_depts
-- ----------------------------
DROP TABLE IF EXISTS `lqz_roles_depts`;
CREATE TABLE `lqz_roles_depts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roles_id` bigint(20) NOT NULL,
  `dept_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_roles_depts_roles_id_dept_id_ae9c29af_uniq` (`roles_id`,`dept_id`),
  KEY `lqz_roles_depts_dept_id_8e794f08_fk_lqz_dept_id` (`dept_id`),
  CONSTRAINT `lqz_roles_depts_dept_id_8e794f08_fk_lqz_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `lqz_dept` (`id`),
  CONSTRAINT `lqz_roles_depts_roles_id_ad60d386_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_roles_depts
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_roles_menus
-- ----------------------------
DROP TABLE IF EXISTS `lqz_roles_menus`;
CREATE TABLE `lqz_roles_menus` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `roles_id` bigint(20) NOT NULL,
  `menu_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_roles_menus_roles_id_menu_id_909659bb_uniq` (`roles_id`,`menu_id`),
  KEY `lqz_roles_menus_menu_id_b0abd116_fk_lqz_menu_id` (`menu_id`),
  CONSTRAINT `lqz_roles_menus_menu_id_b0abd116_fk_lqz_menu_id` FOREIGN KEY (`menu_id`) REFERENCES `lqz_menu` (`id`),
  CONSTRAINT `lqz_roles_menus_roles_id_34bb1dde_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_roles_menus
-- ----------------------------
BEGIN;
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (1, 1, 1);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (2, 1, 2);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (3, 1, 3);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (4, 1, 4);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (5, 1, 5);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (6, 1, 6);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (7, 1, 7);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (8, 1, 8);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (9, 1, 9);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (10, 1, 10);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (11, 1, 11);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (12, 1, 12);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (13, 1, 13);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (14, 1, 14);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (15, 1, 15);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (16, 1, 16);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (17, 1, 17);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (18, 1, 18);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (19, 1, 19);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (20, 1, 20);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (21, 1, 21);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (22, 1, 22);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (23, 1, 23);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (41, 4, 24);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (42, 4, 34);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (43, 4, 36);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (45, 4, 37);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (46, 4, 38);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (47, 4, 39);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (48, 4, 42);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (49, 4, 43);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (44, 4, 44);
INSERT INTO `lqz_roles_menus` (`id`, `roles_id`, `menu_id`) VALUES (50, 4, 45);
COMMIT;

-- ----------------------------
-- Table structure for lqz_user
-- ----------------------------
DROP TABLE IF EXISTS `lqz_user`;
CREATE TABLE `lqz_user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(150) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL,
  `nick_name` varchar(32) DEFAULT NULL,
  `gender` varchar(16) DEFAULT NULL,
  `phone` varchar(11) DEFAULT NULL,
  `avatar` varchar(100) NOT NULL,
  `enabled` tinyint(1) NOT NULL,
  `create_time` datetime(6) DEFAULT NULL,
  `update_time` datetime(6) DEFAULT NULL,
  `create_by` varchar(32) DEFAULT NULL,
  `update_by` varchar(32) DEFAULT NULL,
  `is_delete` tinyint(1) NOT NULL,
  `pwd_reset_time` datetime(6) NOT NULL,
  `is_login` tinyint(1) NOT NULL,
  `dept_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`),
  UNIQUE KEY `nick_name` (`nick_name`),
  UNIQUE KEY `phone` (`phone`),
  KEY `lqz_user_dept_id_25de3b75_fk_lqz_dept_id` (`dept_id`),
  CONSTRAINT `lqz_user_dept_id_25de3b75_fk_lqz_dept_id` FOREIGN KEY (`dept_id`) REFERENCES `lqz_dept` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_user
-- ----------------------------
BEGIN;
INSERT INTO `lqz_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `nick_name`, `gender`, `phone`, `avatar`, `enabled`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `pwd_reset_time`, `is_login`, `dept_id`) VALUES (1, 'pbkdf2_sha256$260000$D046Tx2QYXaItCTNpaUTdA$bqZv5xmnhos5Yo6eNeA7xjuzfIti+Y1n3rQMSjSa3sI=', NULL, 1, 'root', '', '', '5@qq.com', 1, 1, '2022-12-18 08:13:45.117479', '超级管理员', '2', '18953675224', 'avatar/default.png', 1, '2022-12-18 08:13:45.314476', '2023-03-05 11:32:16.956836', NULL, NULL, 0, '2022-12-18 08:13:45.314531', 0, NULL);
INSERT INTO `lqz_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `nick_name`, `gender`, `phone`, `avatar`, `enabled`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `pwd_reset_time`, `is_login`, `dept_id`) VALUES (2, 'pbkdf2_sha256$260000$WzEp6ARwzEmIIPUpWTg8jL$M/d754rX2JxluI5VyXNVfp2xJBZy/FBR/cYD/GkaM7U=', NULL, 1, 'lqz', '', '', '4@qq.com', 1, 1, '2022-12-18 08:13:45.117479', '小白', '2', '18953675222', 'avatar/default.png', 1, '2022-12-18 08:13:45.314476', '2023-02-26 17:23:20.855715', NULL, NULL, 0, '2022-12-18 08:13:45.314531', 0, NULL);
INSERT INTO `lqz_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`, `nick_name`, `gender`, `phone`, `avatar`, `enabled`, `create_time`, `update_time`, `create_by`, `update_by`, `is_delete`, `pwd_reset_time`, `is_login`, `dept_id`) VALUES (8, 'pbkdf2_sha256$260000$XzMCsK7MKhMvrh7jdyPJJ0$0p5VlCnvT8o00qgXTdTA4ib7L5ss7CzwAEFo3vsxgas=', NULL, 0, 'zhangsan', '', '', '44@qq.com', 0, 1, '2023-03-05 17:01:00.754478', '张三', '1', '123344455', 'avatar/default.png', 1, '2023-03-05 17:01:00.754989', '2023-03-05 17:01:26.725180', NULL, NULL, 0, '2023-03-05 17:01:00.755023', 0, 2);
COMMIT;

-- ----------------------------
-- Table structure for lqz_user_groups
-- ----------------------------
DROP TABLE IF EXISTS `lqz_user_groups`;
CREATE TABLE `lqz_user_groups` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_user_groups_userinfo_id_group_id_523aa2ce_uniq` (`userinfo_id`,`group_id`),
  KEY `lqz_user_groups_group_id_85fcfed3_fk_auth_group_id` (`group_id`),
  CONSTRAINT `lqz_user_groups_group_id_85fcfed3_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `lqz_user_groups_userinfo_id_cbd65edd_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_user_groups
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_user_user_permissions
-- ----------------------------
DROP TABLE IF EXISTS `lqz_user_user_permissions`;
CREATE TABLE `lqz_user_user_permissions` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_user_user_permission_userinfo_id_permission_i_4d44706a_uniq` (`userinfo_id`,`permission_id`),
  KEY `lqz_user_user_permis_permission_id_30ccdfa6_fk_auth_perm` (`permission_id`),
  CONSTRAINT `lqz_user_user_permis_permission_id_30ccdfa6_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `lqz_user_user_permissions_userinfo_id_7e17251e_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_user_user_permissions
-- ----------------------------
BEGIN;
COMMIT;

-- ----------------------------
-- Table structure for lqz_users_jobs
-- ----------------------------
DROP TABLE IF EXISTS `lqz_users_jobs`;
CREATE TABLE `lqz_users_jobs` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `job_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_users_jobs_userinfo_id_job_id_c7e9f3c7_uniq` (`userinfo_id`,`job_id`),
  KEY `lqz_users_jobs_job_id_9a99b906_fk_lqz_job_id` (`job_id`),
  CONSTRAINT `lqz_users_jobs_job_id_9a99b906_fk_lqz_job_id` FOREIGN KEY (`job_id`) REFERENCES `lqz_job` (`id`),
  CONSTRAINT `lqz_users_jobs_userinfo_id_7b41e7f5_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_users_jobs
-- ----------------------------
BEGIN;
INSERT INTO `lqz_users_jobs` (`id`, `userinfo_id`, `job_id`) VALUES (6, 8, 2);
COMMIT;

-- ----------------------------
-- Table structure for lqz_users_roles
-- ----------------------------
DROP TABLE IF EXISTS `lqz_users_roles`;
CREATE TABLE `lqz_users_roles` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `userinfo_id` bigint(20) NOT NULL,
  `roles_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `lqz_users_roles_userinfo_id_roles_id_a3790c91_uniq` (`userinfo_id`,`roles_id`),
  KEY `lqz_users_roles_roles_id_46de236c_fk_lqz_role_id` (`roles_id`),
  CONSTRAINT `lqz_users_roles_roles_id_46de236c_fk_lqz_role_id` FOREIGN KEY (`roles_id`) REFERENCES `lqz_role` (`id`),
  CONSTRAINT `lqz_users_roles_userinfo_id_27e3c901_fk_lqz_user_id` FOREIGN KEY (`userinfo_id`) REFERENCES `lqz_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of lqz_users_roles
-- ----------------------------
BEGIN;
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (9, 1, 1);
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (1, 2, 1);
INSERT INTO `lqz_users_roles` (`id`, `userinfo_id`, `roles_id`) VALUES (10, 8, 4);
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
