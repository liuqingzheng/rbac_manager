from rest_framework.mixins import ListModelMixin, RetrieveModelMixin, CreateModelMixin, DestroyModelMixin, \
    UpdateModelMixin
from utils.common_response import APIResponse
from rest_framework.decorators import action

class CommonListModelMixin(ListModelMixin):
    def list(self, request, *args, **kwargs):
        res = super().list(request, *args, **kwargs)
        return APIResponse(data=res.data)


class CommonRetrieveModelMixin(RetrieveModelMixin):
    def retrieve(self, request, *args, **kwargs):
        res = super().retrieve(request, *args, **kwargs)
        return APIResponse(data=res.data)


class CommonCreateModelMixin(CreateModelMixin):
    def create(self, request, *args, **kwargs):
        super().create(request, *args, **kwargs)
        return APIResponse(msg='创建成功')


class CommonDestroyModelMixin(DestroyModelMixin):
    def destroy(self, request, *args, **kwargs):
        super().destroy(request, *args, **kwargs)
        return APIResponse(msg='删除成功')

    @action(methods=['POST'], detail=False)
    def batch_delete(self, request, *args, **kwargs):
        print(request.data.get('ids'))
        self.get_queryset().filter(id__in=request.data.get('ids', [])).delete()
        return APIResponse(msg='删除成功')


class CommonUpdateModelMixin(UpdateModelMixin):
    def update(self, request, *args, **kwargs):
        super().update(request, *args, **kwargs)
        return APIResponse(msg='修改成功')
