from django.db import models


class BaseModel(models.Model):
    create_time = models.DateTimeField(auto_now_add=True, verbose_name='创建时间', blank=True, null=True)
    update_time = models.DateTimeField(auto_now=True, verbose_name='更新时间', blank=True, null=True)
    create_by = models.CharField(max_length=32, verbose_name='创建者', blank=True, null=True)
    update_by = models.CharField(max_length=32, verbose_name='更新者', blank=True, null=True)
    is_delete = models.BooleanField(default=False, verbose_name='是否删除')

    class Meta:
        abstract = True
