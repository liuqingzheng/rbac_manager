from rest_framework.pagination import PageNumberPagination as CommonPageNumberPagination


class PageNumberPagination(CommonPageNumberPagination):
    page_size = 5
    page_query_param = 'page'
    max_page_size = 10
