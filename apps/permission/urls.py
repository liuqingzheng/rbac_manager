from django.urls import path
from rest_framework.routers import SimpleRouter
from . import views
router = SimpleRouter()
router.register('menu',views.PermissionMenuView,'menu')
router.register('roles',views.PermissionRolesView,'roles')
router.register('dept',views.PermissionDeptView,'dept')
router.register('job',views.PermissionJobView,'job')
urlpatterns = [
]
urlpatterns += router.urls
