from rest_framework import serializers
from .models import Roles, Menu, Job, Dept


class RolesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Roles
        fields = '__all__'
        extra_kwargs = {
            'menus': {'allow_null': True, 'required': False},
            'depts': {'allow_null': True, 'required': False},
        }


# 给用户选择部门页面使用的序列化类
class DeptUserSerializer(serializers.ModelSerializer):
    label = serializers.CharField(source='name')

    class Meta:
        model = Dept
        fields = ['id', 'label', 'children']

    # 递归序列化
    children = serializers.SerializerMethodField()

    def get_children(self, obj):
        return DeptUserSerializer(instance=obj.dept_set.all(), many=True).data


class DeptSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dept
        fields = ['id', 'name', 'dept_sort', 'enabled', 'create_time', 'children']

    # 递归序列化
    children = serializers.SerializerMethodField()

    def get_children(self, obj):
        return DeptSerializer(instance=obj.dept_set.all(), many=True).data


class DeptCreateUpdateRetrieveSerializer(serializers.ModelSerializer):
    pid_id = serializers.IntegerField(default=None, required=False, allow_null=True)
    # 只给序列化用
    type = serializers.SerializerMethodField(read_only=True)

    def get_type(self, obj):
        if obj.pid_id:
            return 1
        else:
            return 0

    class Meta:
        model = Dept
        fields = ['id', 'name', 'dept_sort', 'enabled', 'pid_id', 'create_by', 'type']
        extra_kwargs = {
            'create_by': {'default': 'admin'}
        }


class JobSerializer(serializers.ModelSerializer):
    class Meta:
        model = Job
        fields = '__all__'


class MenuOnlySerializer(serializers.ModelSerializer):
    label = serializers.CharField(source='title')

    class Meta:
        model = Menu
        fields = ['id', 'label', 'children']

    # 递归序列化
    children = serializers.SerializerMethodField()

    def get_children(self, obj):
        return MenuOnlySerializer(instance=obj.menu_set.all(), many=True).data


class MenuSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        # id,名字，菜单图标，菜单类型，菜单地址path,菜单组件，菜单排序，权限编码，是否显示
        fields = ['id', 'name', 'icon', 'type', 'path', 'component', 'menu_sort', 'permission', 'title', 'children',
                  'hidden', 'pid', 'is_menu']

    # 递归序列化
    children = serializers.SerializerMethodField()

    def get_children(self, obj):
        return MenuSerializer(instance=obj.menu_set.all(), many=True).data


class MenuPermissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Menu
        fields = ['name', 'title', 'icon', 'path', 'component', 'children']

    # 递归序列化
    children = serializers.SerializerMethodField()

    def get_children(self, obj):
        # 只要目录和菜单
        menus_ids = self.context.get('menus_ids')
        if menus_ids:
            return MenuSerializer(instance=obj.menu_set.filter(type=1, id__in=menus_ids), many=True).data
        else:
            return MenuSerializer(instance=obj.menu_set.filter(type=1), many=True).data
