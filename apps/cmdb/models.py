from django.db import models


# IDC机房
class IDC(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name="机房名称")
    city = models.CharField(max_length=20, verbose_name="城市")
    provider = models.CharField(max_length=30, verbose_name="运营商")
    note = models.TextField(blank=True, null=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = "lqz_cmdb_idc"  # 定义表名
        verbose_name_plural = "IDC机房"


# 分组
class ServerGroup(models.Model):
    name = models.CharField(max_length=30, unique=True, verbose_name="分组名称")
    note = models.TextField(blank=True, null=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = "lqz_cmdb_server_group"
        verbose_name_plural = "主机分组"

    def __str__(self):
        return self.name


# 云服务器信息
class CloudServer(models.Model):
    idc = models.ForeignKey(IDC, on_delete=models.DO_NOTHING, verbose_name="IDC机房")            # 一对多
    server_group = models.ManyToManyField(ServerGroup, default="Default", verbose_name="主机分组")    # 多对多
    credential = models.ForeignKey('Credential', on_delete=models.DO_NOTHING, blank=True, null=True, verbose_name="凭据")   # 一对多
    name = models.CharField(max_length=30, blank=True, verbose_name="名称")
    hostname = models.CharField(max_length=30, unique=True, verbose_name="主机名")
    ssh_ip = models.GenericIPAddressField(verbose_name="远程连接IP")
    ssh_port = models.IntegerField(verbose_name="远程连接端口")
    note = models.TextField(blank=True, null=True, verbose_name="备注")
    machine_type = models.CharField(max_length=30, blank=True, choices=(('windows','windows'),('linux','linux')), default='linux', verbose_name="机器类型")
    os_version = models.CharField(max_length=50, blank=True, null=True, verbose_name="系统版本")
    public_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="公网IP")
    private_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="内网IP")
    cpu_num = models.CharField(max_length=10, blank=True, null=True, verbose_name="CPU")
    memory = models.CharField(max_length=30, blank=True, null=True, verbose_name="内存")
    disk = models.JSONField(max_length=200,blank=True, null=True, verbose_name="硬盘")
    network = models.CharField(max_length=200, blank=True, null=True, verbose_name="带宽")
    put_shelves_date = models.DateField(null=True, blank=True, verbose_name="上架日期")
    off_shelves_date = models.DateField(null=True, blank=True, verbose_name="下架日期")
    expire_datetime = models.DateTimeField(blank=True, null=True, verbose_name="租约过期时间")
    is_verified = models.CharField(max_length=10, blank=True, choices=(('verified','已验证'),('unverified','未验证')), default='unverified', verbose_name="SSH验证状态")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")

    class Meta:
        db_table = "lqz_cmdb_cloud_server"
        verbose_name_plural = "云主机管理"

    def __str__(self):
        return self.hostname


# 物理服务器信息
class PhysicalServer(models.Model):
    idc = models.ForeignKey(IDC, on_delete=models.DO_NOTHING, verbose_name="IDC机房")            # 一对多
    server_group = models.ManyToManyField(ServerGroup, default="Default", verbose_name="主机分组")    # 多对多
    credential = models.ForeignKey('Credential', on_delete=models.DO_NOTHING, blank=True, null=True, verbose_name="凭据")   # 一对多

    name = models.CharField(max_length=30, blank=True, verbose_name="名称")
    hostname = models.CharField(max_length=30, unique=True, verbose_name="主机名")
    ssh_ip = models.GenericIPAddressField(verbose_name="远程连接IP")
    ssh_port = models.IntegerField(verbose_name="远程连接端口")
    note = models.TextField(blank=True, null=True, verbose_name="备注")
    machine_type = models.CharField(max_length=30, blank=True, choices=(('windows','windows'),('linux','linux'),('vmware','vmware')), default='linux', verbose_name="机器类型")
    asset_code = models.CharField(max_length=50, blank=True, null=True, verbose_name="资产编码")
    os_version = models.CharField(max_length=50, blank=True, null=True, verbose_name="系统版本")
    public_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="公网IP")
    private_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="内网IP")
    cpu_num = models.CharField(max_length=10, blank=True, null=True, verbose_name="CPU")
    cpu_model = models.CharField(max_length=100, blank=True, null=True, verbose_name="CPU型号")
    memory = models.CharField(max_length=30, blank=True, null=True, verbose_name="内存")
    disk = models.JSONField(max_length=200,blank=True, null=True, verbose_name="硬盘")
    network = models.CharField(max_length=200, blank=True, null=True, verbose_name="带宽")
    put_shelves_date = models.DateField(null=True, blank=True, verbose_name="上架日期")
    off_shelves_date = models.DateField(null=True, blank=True, verbose_name="下架日期")
    is_verified = models.CharField(max_length=10, blank=True, choices=(('verified','已验证'),('unverified','未验证')), default='unverified', verbose_name="SSH验证状态",null=True)
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间",null=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间",null=True)

    class Meta:
        db_table = "lqz_cmdb_physical_server"
        verbose_name_plural = "物理机管理"

    def __str__(self):
        return self.hostname


# 虚拟设备信息
class VirtualServer(models.Model):
    idc = models.ForeignKey(IDC, on_delete=models.DO_NOTHING, verbose_name="IDC机房")            # 一对多
    server_group = models.ManyToManyField(ServerGroup, default="Default", verbose_name="主机分组")    # 多对多
    credential = models.ForeignKey('Credential', on_delete=models.DO_NOTHING, blank=True, null=True, verbose_name="凭据")   # 一对多

    vm_host = models.ForeignKey(PhysicalServer, on_delete=models.DO_NOTHING, verbose_name="虚拟主机")  # 一对多
    name = models.CharField(max_length=30, blank=True, verbose_name="名称")
    hostname = models.CharField(max_length=30, unique=True, verbose_name="主机名")
    ssh_ip = models.GenericIPAddressField(verbose_name="远程连接IP")
    ssh_port = models.IntegerField(verbose_name="远程连接端口")
    note = models.TextField(blank=True, null=True, verbose_name="备注")
    machine_type = models.CharField(max_length=30, blank=True, choices=(('windows','windows'),('linux','linux')), default='linux', verbose_name="机器类型",null=True)
    os_version = models.CharField(max_length=50, blank=True, null=True, verbose_name="系统版本")
    public_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="公网IP")
    private_ip = models.JSONField(max_length=100, blank=True, null=True, verbose_name="内网IP")
    cpu_num = models.CharField(max_length=10, blank=True, null=True, verbose_name="CPU")
    cpu_model = models.CharField(max_length=100, blank=True, null=True, verbose_name="CPU型号")
    memory = models.CharField(max_length=30, blank=True, null=True, verbose_name="内存")
    disk = models.JSONField(max_length=200,blank=True, null=True, verbose_name="硬盘")
    network = models.CharField(max_length=200, blank=True, null=True, verbose_name="带宽")
    put_shelves_date = models.DateField(null=True, blank=True, verbose_name="上架日期")
    off_shelves_date = models.DateField(null=True, blank=True, verbose_name="下架日期")
    is_verified = models.CharField(max_length=10, blank=True, choices=(('verified','已验证'),('unverified','未验证')), default='unverified', verbose_name="SSH验证状态",null=True)
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间",null=True)
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间",null=True)

    class Meta:
        verbose_name = '虚拟设备'
        verbose_name_plural = verbose_name
        db_table = "lqz_cmdb_virtual_server"


# 网络设备表
class NetWorkServer(models.Model):
    host_ip = models.CharField(max_length=40, verbose_name='网络设备ip')
    host_name = models.CharField(max_length=10, verbose_name='网络设备名')
    sn_key = models.CharField(max_length=256, verbose_name="SN－设备的唯一标识", default="")

    class Meta:
        verbose_name = '网络设备表'
        verbose_name_plural = verbose_name
        db_table = "lqz_cmdb_network_server"


class OtherMachine(models.Model):
    ip = models.CharField(max_length=40, verbose_name='设备ip')
    sn_key = models.CharField(max_length=256, verbose_name='设备的唯一标识')
    machine_name = models.CharField(max_length=20, verbose_name='设备名称')
    note = models.TextField(default='', verbose_name='备注')

    class Meta:
        verbose_name = '其它设备表'
        verbose_name_plural = verbose_name
        db_table = 'lqz_cmdb_other_machine'


# 凭据管理
class Credential(models.Model):
    auth_choice = (
        (1, "密码"),
        (2, "秘钥")
    )
    name = models.CharField(max_length=30, verbose_name="凭据名称")
    username = models.CharField(max_length=20, verbose_name="用户名")
    auth_mode = models.IntegerField(choices=auth_choice, default=1, verbose_name="认证方式")
    password = models.CharField(max_length=50, blank=True, verbose_name="密码")
    private_key = models.TextField(blank=True, verbose_name="私钥")
    note = models.TextField(blank=True, verbose_name="备注")
    create_time = models.DateTimeField(auto_now_add=True, verbose_name="创建时间")
    update_time = models.DateTimeField(auto_now=True, verbose_name="更新时间")

    class Meta:
        db_table = "lqz_cmdb_credential"
        verbose_name_plural = "凭据管理"

    def __str__(self):
        return self.name
