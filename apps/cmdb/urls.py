from rest_framework.routers import SimpleRouter
from . import views

router = SimpleRouter()
router.register('idc', views.IDCView, 'idc')
router.register('group', views.ServerGroupView, 'group')
router.register('credential', views.CredentialView, 'credential')
router.register('networkserver', views.NetWorkServerView, 'networkserver')
router.register('othermachine', views.OtherMachineView, 'othermachine')
router.register('physicalserver', views.PhysicalServerView, 'physicalserver')
urlpatterns = [
]
urlpatterns += router.urls
