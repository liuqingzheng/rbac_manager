from .models import IDC, ServerGroup, Credential, OtherMachine, NetWorkServer,PhysicalServer,VirtualServer
from rest_framework import serializers


class IDCSerializer(serializers.ModelSerializer):
    class Meta:
        model = IDC
        fields = '__all__'


class ServerGroupSerializer(serializers.ModelSerializer):
    class Meta:
        model = ServerGroup
        fields = '__all__'


class CredentialSerializer(serializers.ModelSerializer):
    class Meta:
        model = Credential
        fields = '__all__'


class OtherMachineSerializer(serializers.ModelSerializer):
    class Meta:
        model = OtherMachine
        fields = '__all__'


class NetWorkServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = NetWorkServer
        fields = '__all__'


class PhysicalServerSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhysicalServer
        fields = '__all__'
