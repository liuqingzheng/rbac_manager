from django.db import models
from utils.common_models import BaseModel
from django.contrib.auth.hashers import check_password, make_password


class UserInfo(BaseModel):
    USERNAME_FIELD = 'username'
    username = models.CharField(max_length=150, unique=True, verbose_name='用户名')
    password = models.CharField(max_length=128, verbose_name='用户密码')
    is_active = models.BooleanField(default=True, verbose_name='是否是活跃用户')
    email = models.EmailField(blank=True, verbose_name='邮箱', null=True, )
    """
    id, 和部门是一对多，昵称，性别，手机号，头像，密码， 状态， 创建者，更新者，创建时间，更新时间，修改密码时间
    """
    nick_name = models.CharField(max_length=32, verbose_name='用户昵称', blank=True, null=True, unique=True)
    gender = models.CharField(max_length=16, verbose_name='性别', blank=True, null=True)
    phone = models.CharField(max_length=11, verbose_name='电话号码', blank=True, null=True, unique=True)
    avatar = models.ImageField(upload_to='avatar/%Y/%m', default='avatar/default.png', verbose_name='头像')
    enabled = models.BooleanField(default=True, verbose_name='是否启用?状态：1启用、0禁用')
    is_superuser = models.BooleanField(default=False, verbose_name='是否是超级用户')
    pwd_reset_time = models.DateTimeField(auto_now_add=True, verbose_name='修改密码的时间')
    is_login = models.BooleanField(default=False, verbose_name='是否登录')

    # 关联字段
    dept = models.ForeignKey(to='permission.Dept', on_delete=models.SET_NULL, verbose_name='部门名字', null=True)
    job = models.ManyToManyField(to='permission.Job', verbose_name='用户和岗位的关联表', db_table='lqz_users_jobs')
    roles = models.ManyToManyField(to='permission.Roles', verbose_name='用户和角色的关联表', db_table='lqz_users_roles')

    class Meta:
        db_table = 'lqz_user'
        verbose_name = '用户表'
        verbose_name_plural = verbose_name

    def __str__(self):
        return self.username

    @staticmethod
    def make_password(password, salt=None, hasher='default'):
        return make_password(password, salt, hasher)

    def check_password(self, raw_password):
        return check_password(raw_password, self.password)



class OnlineUser(BaseModel):
    address = models.CharField(max_length=64, verbose_name='在线用户登录地址', blank=True, null=True)
    browser = models.CharField(max_length=128, verbose_name='浏览器', blank=True, null=True)
    ip = models.CharField(max_length=64, verbose_name='用户登录ip', blank=True, null=True)
    key = models.CharField(max_length=255, verbose_name='存用户token', blank=True, null=True)
    # 关联
    user = models.ForeignKey(to=UserInfo, on_delete=models.SET_NULL, verbose_name='和用户的一对多', null=True)

    class Meta:
        db_table = 'lqz_online_user'
        verbose_name = '在线用户的记录表'
        verbose_name_plural = verbose_name
