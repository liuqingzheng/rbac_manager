from rest_framework.authentication import BaseAuthentication
from rest_framework.exceptions import APIException
from libs.lqz_jwt import TokenManager
from .models import UserInfo


class LoginAuthentication(BaseAuthentication):
    def authenticate(self, request):
        token = request.META.get('HTTP_AUTHORIZATION', None)
        if token:
            payload = TokenManager.get_payload(token)
            user = UserInfo.objects.get(pk=payload.get('id'))
            return user, token
        else:
            raise APIException('没有携带token信息')
