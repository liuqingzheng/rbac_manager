from django.urls import path
from rest_framework.routers import SimpleRouter
from . import views

router = SimpleRouter()
router.register('login', views.UserLoginView, 'login')
router.register('user', views.UserView, 'user')
urlpatterns = [
    # path('admin/', admin.site.urls),
]
urlpatterns += router.urls
